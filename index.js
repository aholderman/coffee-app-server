"use strict";
const express = require("express");
const r = require('rethinkdb');


// Globals
const serverPort = 3000;
const rHost = "localhost";
const rPort = "28015";

// Start server
const app = express();
app.use(express.static(__dirname + "/public"));
const server = app.listen(serverPort, () => {
	console.log("listening on " + serverPort);
})

// Socket.io
var io = require("socket.io")(server);

io.on('connection', function(socket) {
	socket.emit('connection', "Connection successful");
	socket.on('log', function(data) {
		console.log(data);
	})
})

// Open RethinkDB connection
var connection = null;
r.connect({host: rHost, port: rPort}, function(err, conn) {
	if (err) throw err;
	connection = conn;
	console.log("connected to rethinkdb on " + rHost + ":" + rPort)

	// Start RethinkDB feed
	r.table('logs').changes().run(connection, function(err, cursor) {
	    if (err) throw err;
	    cursor.each(function(err, row) {
	        if (err) throw err;
	        console.log(JSON.stringify(row, null, 2));

	        // add socket.io emission
	        io.on('connection', function(socket) {
	        	socket.emit('log', {data: data});
	        })

	    });
	});
});

// TODO: Add redux store